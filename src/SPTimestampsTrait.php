<?php
/**
 * This file is part of the Bevira SharePoint library. This library is based on SharePoint OAuth App Client library by Quetzy Garcia <qgarcia@wearearchitect.com>
 */

namespace Bevira\SharePoint;

trait SPTimestampsTrait
{
    /**
     * Creation Time
     *
     * @access  protected
     * @var     \Carbon\Carbon
     */
    protected $created;

    /**
     * Modification Time
     *
     * @access  protected
     * @var     \Carbon\Carbon
     */
    protected $modified;

    /**
     * Get Creation Time
     *
     * @access  public
     * @return  \Carbon\Carbon
     */
    public function getTimeCreated()
    {
        return $this->created;
    }

    /**
     * Get Modification Time
     *
     * @access  public
     * @return  \Carbon\Carbon
     */
    public function getTimeModified()
    {
        return $this->modified;
    }
}
