<?php
/**
 * Created by PhpStorm.
 * Author: Rain Laansalu
 * Date: 21.05.2020
 * Time: 11:52
 */

namespace Bevira\SharePoint;


class Session
{

    public static function exists($name)
    {
        return isset($_SESSION[$name]);
    }

    public static function put($name, $value)
    {
        return $_SESSION[$name] = $value;
    }

    public static function get($name)
    {
        if($_SESSION[$name]) {
            return $_SESSION[$name];
        }
        return null;
    }

    public static function get_unserialized($name)
    {
        return unserialize(self::get($name));
    }

    public static function put_serialized($name, $value)
    {
        self::put($name, serialize($value));
    }

    public static function delete($name)
    {
        if(self::exists($name)) {
            unset($_SESSION[$name]);
        }
    }
}