<?php
/**
 * This file is part of the Bevira SharePoint library. This library is based on SharePoint OAuth App Client library by Quetzy Garcia <qgarcia@wearearchitect.com>
 */

namespace Bevira\SharePoint;

interface SPFolderInterface extends SPRequesterInterface
{
    /**
     * Get SharePoint Site
     *
     * @access  public
     * @return  SPSite
     */
    public function getSPSite();

    /**
     * Get Relative URL
     *
     * @access  public
     * @param   string $path Path to append to the Relative URL
     * @return  string
     */
    public function getRelativeUrl($path = null);

    /**
     * Get URL
     *
     * @access  public
     * @param   string $path Path to append to the URL
     * @return  string
     */
    public function getUrl($path = null);

    /**
     * Is the folder writable?
     *
     * @access  public
     * @param   bool   $exception Throw exception if not writable?
     * @return  bool
     */
    public function isWritable($exception = false);
}
