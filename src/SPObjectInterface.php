<?php
/**
 * This file is part of the Bevira SharePoint library. This library is based on SharePoint OAuth App Client library by Quetzy Garcia <qgarcia@wearearchitect.com>
 */

namespace Bevira\SharePoint;

interface SPObjectInterface
{
    /**
     * Get an array with the SPObject properties
     *
     * @access  public
     * @return  array
     */
    public function toArray();
}
