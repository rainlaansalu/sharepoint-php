<?php
/**
 * This file is part of the Bevira SharePoint library. This library is based on SharePoint OAuth App Client library by Quetzy Garcia <qgarcia@wearearchitect.com>
 */

namespace Bevira\SharePoint;

trait SPPropertiesTrait
{
    /**
     * SharePoint Type
     *
     * @access  protected
     * @var     string
     */
    protected $type;

    /**
     * SharePoint GUID
     *
     * @access  protected
     * @var     string
     */
    protected $guid;

    /**
     * SharePoint Title
     *
     * @access  protected
     * @var     string
     */
    protected $title;

    /**
     * Get SharePoint Type
     *
     * @access  public
     * @return  string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get SharePoint GUID
     *
     * @access  public
     * @return  string
     */
    public function getGUID()
    {
        return $this->guid;
    }

    /**
     * Get SharePoint Title
     *
     * @access  public
     * @return  string
     */
    public function getTitle()
    {
        return $this->title;
    }
}
