<?php
/**
 * This file is part of the Bevira SharePoint library. This library is based on SharePoint OAuth App Client library by Quetzy Garcia <qgarcia@wearearchitect.com>
 */

namespace Bevira\SharePoint;

interface SPItemInterface
{
    /**
     * Get SharePoint GUID
     *
     * @access  public
     * @return  string
     */
    public function getGUID();

    /**
     * Get SharePoint Title
     *
     * @access  public
     * @return  string
     */
    public function getTitle();
}
