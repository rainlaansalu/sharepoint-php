<?php
/**
 * Created by PhpStorm.
 * Author: Rain
 * Date: 21.05.2020
 * Time: 11:35
 */
session_start();
use Bevira\SharePoint\SPFolder;
use Bevira\SharePoint\SPSite;
use PHPUnit\Framework\TestCase;

final class SPTest extends TestCase
{
    private $Bearer_realm = '';
    private $siteUrl = '';
    private $clientSecret = '';
    private $clientId = '';

    public function testCanGetSite(): void
    {
        $this->assertInstanceOf(
            SPSite::class,
            SPSite::create($this->clientId,$this->clientSecret,$this->siteUrl,$this->Bearer_realm)
        );
    }

    public function testCanGetFolder(): void
    {
        $site = SPSite::create($this->clientId,$this->clientSecret,$this->siteUrl,$this->Bearer_realm);

        $this->assertInstanceOf(
            SPFolder::class,
            SPFolder::getByRelativeUrl($site,'Shared Documents')
        );
    }
}
session_destroy();