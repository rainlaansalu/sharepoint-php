# SharePoint PHP #

The SharePoint OAuth App Client is a PHP library that makes it easy to authenticate via OAuth2 with the SharePoint Online (2013) REST API and work with Lists, Folders, Items, Files and Users.

#Installation

## Composer

``` json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:rainlaansalu/sharepoint-php.git"
        }
    ],
    "require": {
        "bevira/sharepoint-php": "dev-master"
    }
}
```

## Basic usage example
```php
<?php

require 'vendor/autoload.php';

use Bevira\SharePoint\SPFile;
use Bevira\SharePoint\SPFolder;
use Bevira\SharePoint\SPSite;;

try {
        $site = SPSite::create($this->clientId,$this->clientSecret,$this->siteUrl,$this->Bearer_realm);
        $folder = SPFolder::getByRelativeUrl($site,'Shared Documents');
        $file = SPFile::getByName($folder,'test.jpg');
        echo $file->getUrl();();

} catch (SPException $e) {
    // handle exceptions
}
```